﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace BattleSim
{
    public static class RandomNumber
    {
        private static readonly RNGCryptoServiceProvider _generator = new RNGCryptoServiceProvider();

        public static void Shuffle<T>(this T[] Array)
        {
            T Buffer;
            int max = Array.Length - 1;
            for (int i = max - 1; i >= 0; i--)
            {
                int j = Between(0, max);
                Buffer = Array[j];
                Array[j] = Array[i];
                Array[i] = Buffer;
            }
        }

        public static int Between(int minimumValue, int maximumValue)
        {
            byte[] randomNumber = new byte[1];

            _generator.GetBytes(randomNumber);

            double asciiValueOfRandomCharacter = Convert.ToDouble(randomNumber[0]);

            // We are using Math.Max, and substracting 0.00000000001, 
            // to ensure "multiplier" will always be between 0.0 and .99999999999
            // Otherwise, it's possible for it to be "1", which causes problems in our rounding.
            double multiplier = Math.Max(0, (asciiValueOfRandomCharacter / 255d) - 0.00000000001d);

            // We need to add one to the range, to allow for the rounding done with Math.Floor
            int range = maximumValue - minimumValue + 1;

            double randomValueInRange = Math.Floor(multiplier * range);

            return (int)(minimumValue + randomValueInRange);
        }
    }
    public class FastSim
    {
        public class Player
        {
            public Unit[] StartingUnits;
            public Unit[] CurrentUnits;
            public Unit[] BattleEnds = new Unit[0];
            public Unit[] AverageBattleEnd(int Battles)
            {
                Unit[] Averages = Unit.BattleAverage(BattleEnds, Battles);
                Unit[] BattleLosses = Unit.SubtractAlive(StartingUnits, Averages);
                for (int i = 0; i < BattleLosses.Length; i++)
                    Averages[i].Lost = BattleLosses[i].Count;
                return Averages;
            }
            public void Reset()
            {
                StartingUnits = (from Unit in StartingUnits orderby Unit.Name select Unit).ToArray();
                CurrentUnits = Unit.Expand(StartingUnits);
            }
            public bool Aggressive { get { return CurrentUnits.Sum(o => o.AT * o.Count) > 0; } }
            public void RemoveDead()
            {
                Unit.RemoveDead(CurrentUnits);
                CurrentUnits = Unit.Expand(Unit.Condense(CurrentUnits));
            }
            public void Complete()
            {
                CurrentUnits = Unit.Condense(CurrentUnits, StartingUnits);
                BattleEnds = (from unit in Unit.Condense(new Unit[][] { BattleEnds, CurrentUnits }.SelectMany(o => o).ToArray(), StartingUnits) orderby unit.Name select unit).ToArray();
            }
            public void ClearBattles()
            {
                CurrentUnits = new Unit[0];
                BattleEnds = new Unit[0];
                Wins = 0;
                WinTurns = 0;
            }
            public long Wins = 0, WinTurns = 0;
        }
        public Player Offense, Defense;
        public int Turns = 0, Battles = 0;
        public void Reset()
        {
            Battles = 0;
            Turns = 0;
            Offense.ClearBattles();
            Defense.ClearBattles();
        }
        public void RunBattle()
        {
            Battles++;
            Turns = 0;
            Offense.Reset();
            Defense.Reset();
            while (Offense.Aggressive && Defense.Aggressive)
            {
                Attack(Offense, Defense);
                Attack(Defense, Offense);
                Offense.RemoveDead();
                Defense.RemoveDead();
                Turns++;
            }
            if (Offense.Aggressive)
            {
                Offense.Wins++;
                Offense.WinTurns += Turns;
            }
            if (Defense.Aggressive)
            {
                Defense.Wins++;
                Defense.WinTurns += Turns;
            }
            Offense.Complete();
            Defense.Complete();
        }

        private static void Attack(Player Offense, Player Defense)
        {
            for (int i = 0; i < Offense.CurrentUnits.Length; i++)
            {
                Unit AttackUnit = Offense.CurrentUnits[i];
                for (int attacks = 0; attacks < AttackUnit.AT; attacks++)
                {
                    int roll = RandomNumber.Between(0, 100);
                    if (roll <= AttackUnit.OF)
                    {
                        int Index = RandomNumber.Between(0, Defense.CurrentUnits.Length - 1);
                        long DF = Defense.CurrentUnits[Index].DF;
                        roll = RandomNumber.Between(0, 100);
                        if (roll > DF)
                            Defense.CurrentUnits[Index].Dead = 1;
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Unit.LoadUnits();
            System.Windows.Forms.Application.Run(new Simulator());
        }
        public struct Unit
        {
            public Unit(Unit BaseUnit)
            {
                FP = BaseUnit.FP;
                Stealth = BaseUnit.Stealth;
                Max = BaseUnit.Max;
                OF = BaseUnit.OF;
                DF = BaseUnit.DF;
                AT = BaseUnit.AT;
                PD = BaseUnit.PD;
                CA = BaseUnit.CA;
                SP = BaseUnit.SP;
                CPX = BaseUnit.CPX;
                CST = BaseUnit.CST;
                License = BaseUnit.License;
                UID = BaseUnit.UID;
                TurnAvailable = BaseUnit.TurnAvailable;
                ScanRange = BaseUnit.ScanRange;
                Count = BaseUnit.Count;
                Dead = BaseUnit.Dead;
                Lost = -1;
            }
            public static Dictionary<long, string> UIDNames = new Dictionary<long, string>();
            public static Dictionary<string, long> NamesUID = new Dictionary<string, long>();
            public static Dictionary<long, Unit> Units = new Dictionary<long, Unit>();
            public static void LoadUnits()
            {
                string[] Lines = System.IO.File.ReadAllLines("units.csv");
                foreach (string Line in Lines)
                {
                    string[] Variables = Line.Split(',');
                    string Name = Variables[0];
                    long UID = long.Parse(Variables[13]);
                    Units.Add(
                        UID,
                        new Unit()
                        {
                            FP = float.Parse(Variables[1]),
                            Max = long.Parse(Variables[2]),
                            OF = long.Parse(Variables[3]),
                            DF = long.Parse(Variables[4]),
                            AT = long.Parse(Variables[5]),
                            PD = long.Parse(Variables[6]),
                            CA = long.Parse(Variables[7]),
                            SP = long.Parse(Variables[8]),
                            CPX = long.Parse(Variables[9]),
                            CST = long.Parse(Variables[11]),
                            License = long.Parse(Variables[12]),
                            UID = UID,
                            TurnAvailable = long.Parse(Variables[14]),
                            Stealth = int.Parse(Variables[15]) == 1,
                            ScanRange = int.Parse(Variables[16]),
                            Lost = -1
                        }
                    );
                    UIDNames.Add(UID, Name);
                }
            }
            public float FP;
            public bool Stealth;
            public long Max, OF, DF, AT, PD, CA, SP, CPX, CST, License, UID, TurnAvailable, ScanRange;
            public decimal Count, Dead, Lost;
            public decimal Alive { get { return Count - Dead; } }
            public string Name { get { return UIDNames[UID]; } }
            public void RemoveDead()
            {
                Count -= Dead;
                Dead = 0;
            }
            public static void RemoveDead(Unit[] Array)
            {
                for (int i = 0; i < Array.Length; i++)
                    Array[i].RemoveDead();
            }
            public void Fill(Unit[] Array, long Index)
            {
                for (long i = Index; i < Array.Length && i < Index + Count; i++)
                {
                    Array[i] = this;
                    Array[i].Count = 1;
                    Array[i].Dead = 0;
                }
            }
            public static Unit[] Expand(Unit[] Units)
            {
                Unit[] NewUnits = new Unit[(long)Units.Sum(o => Math.Ceiling(o.Count))];
                long Index = 0;
                foreach (Unit Unit in Units)
                {
                    Unit.Fill(NewUnits, Index);
                    for (int DeadProcessed = 0; DeadProcessed < Unit.Dead; NewUnits[Index + DeadProcessed++].Dead = 1) ;
                    Index += (long)Math.Ceiling(Unit.Count);
                }
                return NewUnits;
            }
            public static Unit[] Condense(Unit[] Units, Unit[] Begin = null)
            {
                long[] IDs = Units.Select(o => o.UID).Distinct().ToArray();
                Unit[][] UnitsList = (from id in IDs select (from unit in Units where unit.UID == id select unit).ToArray()).ToArray();
                Unit[] Condensed = (from list in UnitsList
                        select new Unit(list[0])
                        {
                            Count = list.Length > 0 ? list.Sum(o => o.Count) : 0,
                            Dead = list.Length > 0 ? list.Sum(o => o.Dead) : 0
                        }
                       ).ToArray();
                if (Begin == null) return Condensed;
                long[] OldIDs = Begin.Select(o => o.UID).Distinct().ToArray();
                Unit[] MissingUnits = (from id in OldIDs where !IDs.Contains(id) select Unit.Units[id]).ToArray();
                return new Unit[][] { Condensed, MissingUnits }.SelectMany(o => o).ToArray();
            }
            public static Unit[] SubtractAlive(Unit[] UnitsStart, Unit[] UnitsEnd)
            {
                Unit[] Units = new Unit[UnitsStart.Length];
                for (int i = 0; i < Units.Length; i++)
                    Units[i] = new Unit(UnitsStart[i]) { Count = UnitsStart[i].Count - UnitsEnd[i].Count };
                return Units;
            }
            public override string ToString()
            {
                return
                    Count > 0 || Lost >= 0 ?
                        Lost >= 0 ? $"{Name} {Math.Round(Count, 2)}x : -{Math.Round(Lost,2)}" :
                        $"{Name} {Math.Round(Count,2)}x" :
                        Name;
            }
            public static Unit[] BattleAverage(Unit[] Units, int Battles)
            {
                return (from Unit in Units where Unit.UID != 0 select new Unit(Unit) { Count = Unit.Count / Battles }).ToArray();
            }
        }
    }
}