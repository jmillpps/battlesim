﻿namespace BattleSim
{
    partial class Simulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DefenseListBox = new System.Windows.Forms.ListBox();
            this.OffenseListBox = new System.Windows.Forms.ListBox();
            this.OffenseCalculationList = new System.Windows.Forms.ListBox();
            this.DefenseCalculationList = new System.Windows.Forms.ListBox();
            this.CountBox = new System.Windows.Forms.TextBox();
            this.AddOffenseBox = new System.Windows.Forms.Button();
            this.AddDefenseBox = new System.Windows.Forms.Button();
            this.UnitListBox = new System.Windows.Forms.ListBox();
            this.RemoveOffenseBox = new System.Windows.Forms.Button();
            this.RemoveDefenseBox = new System.Windows.Forms.Button();
            this.SubtractOffenseBox = new System.Windows.Forms.Button();
            this.SubtractDefenseBox = new System.Windows.Forms.Button();
            this.RunBox = new System.Windows.Forms.Button();
            this.OffenseSummaryLabel = new System.Windows.Forms.Label();
            this.DefenseSummaryLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // DefenseListBox
            // 
            this.DefenseListBox.FormattingEnabled = true;
            this.DefenseListBox.Location = new System.Drawing.Point(524, 12);
            this.DefenseListBox.Name = "DefenseListBox";
            this.DefenseListBox.Size = new System.Drawing.Size(250, 212);
            this.DefenseListBox.TabIndex = 1;
            this.DefenseListBox.SelectedIndexChanged += new System.EventHandler(this.DefenseListBox_SelectedIndexChanged);
            // 
            // OffenseListBox
            // 
            this.OffenseListBox.FormattingEnabled = true;
            this.OffenseListBox.Location = new System.Drawing.Point(12, 12);
            this.OffenseListBox.Name = "OffenseListBox";
            this.OffenseListBox.Size = new System.Drawing.Size(250, 212);
            this.OffenseListBox.TabIndex = 2;
            this.OffenseListBox.SelectedIndexChanged += new System.EventHandler(this.OffenseListBox_SelectedIndexChanged);
            // 
            // OffenseCalculationList
            // 
            this.OffenseCalculationList.Enabled = false;
            this.OffenseCalculationList.FormattingEnabled = true;
            this.OffenseCalculationList.Location = new System.Drawing.Point(12, 287);
            this.OffenseCalculationList.Name = "OffenseCalculationList";
            this.OffenseCalculationList.Size = new System.Drawing.Size(378, 212);
            this.OffenseCalculationList.TabIndex = 3;
            // 
            // DefenseCalculationList
            // 
            this.DefenseCalculationList.Enabled = false;
            this.DefenseCalculationList.FormattingEnabled = true;
            this.DefenseCalculationList.Location = new System.Drawing.Point(396, 287);
            this.DefenseCalculationList.Name = "DefenseCalculationList";
            this.DefenseCalculationList.Size = new System.Drawing.Size(378, 212);
            this.DefenseCalculationList.TabIndex = 4;
            // 
            // CountBox
            // 
            this.CountBox.Location = new System.Drawing.Point(268, 230);
            this.CountBox.Name = "CountBox";
            this.CountBox.Size = new System.Drawing.Size(250, 20);
            this.CountBox.TabIndex = 5;
            // 
            // AddOffenseBox
            // 
            this.AddOffenseBox.Location = new System.Drawing.Point(184, 228);
            this.AddOffenseBox.Name = "AddOffenseBox";
            this.AddOffenseBox.Size = new System.Drawing.Size(78, 23);
            this.AddOffenseBox.TabIndex = 6;
            this.AddOffenseBox.Text = "<<";
            this.AddOffenseBox.UseVisualStyleBackColor = true;
            this.AddOffenseBox.Click += new System.EventHandler(this.AddOffenseBox_Click);
            // 
            // AddDefenseBox
            // 
            this.AddDefenseBox.Location = new System.Drawing.Point(524, 228);
            this.AddDefenseBox.Name = "AddDefenseBox";
            this.AddDefenseBox.Size = new System.Drawing.Size(78, 23);
            this.AddDefenseBox.TabIndex = 7;
            this.AddDefenseBox.Text = ">>";
            this.AddDefenseBox.UseVisualStyleBackColor = true;
            this.AddDefenseBox.Click += new System.EventHandler(this.AddDefenseBox_Click);
            // 
            // UnitListBox
            // 
            this.UnitListBox.FormattingEnabled = true;
            this.UnitListBox.Location = new System.Drawing.Point(268, 12);
            this.UnitListBox.Name = "UnitListBox";
            this.UnitListBox.Size = new System.Drawing.Size(250, 212);
            this.UnitListBox.TabIndex = 8;
            this.UnitListBox.SelectedIndexChanged += new System.EventHandler(this.UnitListBox_SelectedIndexChanged);
            // 
            // RemoveOffenseBox
            // 
            this.RemoveOffenseBox.Location = new System.Drawing.Point(12, 228);
            this.RemoveOffenseBox.Name = "RemoveOffenseBox";
            this.RemoveOffenseBox.Size = new System.Drawing.Size(78, 23);
            this.RemoveOffenseBox.TabIndex = 9;
            this.RemoveOffenseBox.Text = "X";
            this.RemoveOffenseBox.UseVisualStyleBackColor = true;
            this.RemoveOffenseBox.Click += new System.EventHandler(this.RemoveOffenseBox_Click);
            // 
            // RemoveDefenseBox
            // 
            this.RemoveDefenseBox.Location = new System.Drawing.Point(696, 228);
            this.RemoveDefenseBox.Name = "RemoveDefenseBox";
            this.RemoveDefenseBox.Size = new System.Drawing.Size(78, 23);
            this.RemoveDefenseBox.TabIndex = 10;
            this.RemoveDefenseBox.Text = "X";
            this.RemoveDefenseBox.UseVisualStyleBackColor = true;
            this.RemoveDefenseBox.Click += new System.EventHandler(this.RemoveDefenseBox_Click);
            // 
            // SubtractOffenseBox
            // 
            this.SubtractOffenseBox.Location = new System.Drawing.Point(98, 228);
            this.SubtractOffenseBox.Name = "SubtractOffenseBox";
            this.SubtractOffenseBox.Size = new System.Drawing.Size(78, 23);
            this.SubtractOffenseBox.TabIndex = 11;
            this.SubtractOffenseBox.Text = ">>";
            this.SubtractOffenseBox.UseVisualStyleBackColor = true;
            this.SubtractOffenseBox.Click += new System.EventHandler(this.SubtractOffenseBox_Click);
            // 
            // SubtractDefenseBox
            // 
            this.SubtractDefenseBox.Location = new System.Drawing.Point(612, 228);
            this.SubtractDefenseBox.Name = "SubtractDefenseBox";
            this.SubtractDefenseBox.Size = new System.Drawing.Size(75, 23);
            this.SubtractDefenseBox.TabIndex = 12;
            this.SubtractDefenseBox.Text = "<<";
            this.SubtractDefenseBox.UseVisualStyleBackColor = true;
            this.SubtractDefenseBox.Click += new System.EventHandler(this.SubtractDefenseBox_Click);
            // 
            // RunBox
            // 
            this.RunBox.Location = new System.Drawing.Point(12, 505);
            this.RunBox.Name = "RunBox";
            this.RunBox.Size = new System.Drawing.Size(762, 23);
            this.RunBox.TabIndex = 14;
            this.RunBox.Text = "Run";
            this.RunBox.UseVisualStyleBackColor = true;
            this.RunBox.Click += new System.EventHandler(this.RunBox_Click);
            // 
            // OffenseSummaryLabel
            // 
            this.OffenseSummaryLabel.AutoSize = true;
            this.OffenseSummaryLabel.Location = new System.Drawing.Point(9, 254);
            this.OffenseSummaryLabel.Name = "OffenseSummaryLabel";
            this.OffenseSummaryLabel.Size = new System.Drawing.Size(276, 13);
            this.OffenseSummaryLabel.TabIndex = 15;
            this.OffenseSummaryLabel.Text = "T: XX -ULT: XXXX -CPXL: XXXXX -ATL: XXXX -UL: XXX";
            // 
            // DefenseSummaryLabel
            // 
            this.DefenseSummaryLabel.AutoSize = true;
            this.DefenseSummaryLabel.Location = new System.Drawing.Point(498, 254);
            this.DefenseSummaryLabel.Name = "DefenseSummaryLabel";
            this.DefenseSummaryLabel.Size = new System.Drawing.Size(276, 13);
            this.DefenseSummaryLabel.TabIndex = 16;
            this.DefenseSummaryLabel.Text = "T: XX -ULT: XXXX -CPXL: XXXXX -ATL: XXXX -UL: XXX";
            // 
            // Simulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 539);
            this.Controls.Add(this.DefenseSummaryLabel);
            this.Controls.Add(this.OffenseSummaryLabel);
            this.Controls.Add(this.RunBox);
            this.Controls.Add(this.SubtractDefenseBox);
            this.Controls.Add(this.SubtractOffenseBox);
            this.Controls.Add(this.RemoveDefenseBox);
            this.Controls.Add(this.RemoveOffenseBox);
            this.Controls.Add(this.UnitListBox);
            this.Controls.Add(this.AddDefenseBox);
            this.Controls.Add(this.AddOffenseBox);
            this.Controls.Add(this.CountBox);
            this.Controls.Add(this.DefenseCalculationList);
            this.Controls.Add(this.OffenseCalculationList);
            this.Controls.Add(this.OffenseListBox);
            this.Controls.Add(this.DefenseListBox);
            this.Name = "Simulator";
            this.Text = "Simulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox DefenseListBox;
        private System.Windows.Forms.ListBox OffenseListBox;
        private System.Windows.Forms.ListBox OffenseCalculationList;
        private System.Windows.Forms.ListBox DefenseCalculationList;
        private System.Windows.Forms.TextBox CountBox;
        private System.Windows.Forms.Button AddOffenseBox;
        private System.Windows.Forms.Button AddDefenseBox;
        private System.Windows.Forms.ListBox UnitListBox;
        private System.Windows.Forms.Button RemoveOffenseBox;
        private System.Windows.Forms.Button RemoveDefenseBox;
        private System.Windows.Forms.Button SubtractOffenseBox;
        private System.Windows.Forms.Button SubtractDefenseBox;
        private System.Windows.Forms.Button RunBox;
        private System.Windows.Forms.Label OffenseSummaryLabel;
        private System.Windows.Forms.Label DefenseSummaryLabel;
    }
}