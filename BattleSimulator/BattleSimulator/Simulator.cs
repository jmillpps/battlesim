﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleSim
{
    public partial class Simulator : Form
    {
        public Simulator()
        {
            InitializeComponent();
            UnitListBox.Sorted = true;
            OffenseListBox.Sorted = true;
            DefenseListBox.Sorted = true;
            foreach (FastSim.Unit Unit in FastSim.Unit.Units.Select(o => o.Value))
                UnitListBox.Items.Add(Unit);
        }

        private void AddOffenseBox_Click(object sender, EventArgs e)
        {
            int Count = 0;
            if (int.TryParse(CountBox.Text, out Count) && Count > 0 && UnitListBox.SelectedIndex >= 0)
            {
                FastSim.Unit SelectedUnit = (FastSim.Unit)UnitListBox.SelectedItem;
                for (int i = 0; i < OffenseListBox.Items.Count; i++)
                {
                    FastSim.Unit Unit = (FastSim.Unit)OffenseListBox.Items[i];
                    if (Unit.UID == SelectedUnit.UID)
                    {
                        OffenseListBox.Items[i] = new FastSim.Unit(Unit) { Count = Unit.Count + Count };
                        OffenseListBox.SelectedIndex = i;
                        return;
                    }
                }
                OffenseListBox.SelectedIndex = OffenseListBox.Items.Add(new FastSim.Unit(SelectedUnit) { Count = Count });
            }
        }

        private void AddDefenseBox_Click(object sender, EventArgs e)
        {
            int Count = 0;
            if (int.TryParse(CountBox.Text, out Count) && Count > 0 && UnitListBox.SelectedIndex >= 0)
            {
                FastSim.Unit SelectedUnit = (FastSim.Unit)UnitListBox.SelectedItem;
                for (int i = 0; i < DefenseListBox.Items.Count; i++)
                {
                    FastSim.Unit Unit = (FastSim.Unit)DefenseListBox.Items[i];
                    if (Unit.UID == SelectedUnit.UID)
                    {
                        DefenseListBox.Items[i] = new FastSim.Unit(Unit) { Count = Unit.Count + Count };
                        DefenseListBox.SelectedIndex = i;
                        return;
                    }
                }
                DefenseListBox.SelectedIndex = DefenseListBox.Items.Add(new FastSim.Unit(SelectedUnit) { Count = Count });
            }
        }

        private void RemoveOffenseBox_Click(object sender, EventArgs e)
        {
            int SelectedIndex = OffenseListBox.SelectedIndex;
            if (SelectedIndex >= 0)
                OffenseListBox.Items.RemoveAt(SelectedIndex);
            if (SelectedIndex < OffenseListBox.Items.Count)
                OffenseListBox.SelectedIndex = SelectedIndex;
            else
            {
                SelectedIndex -= 1;
                if (SelectedIndex < OffenseListBox.Items.Count)
                    OffenseListBox.SelectedIndex = SelectedIndex;
            }
        }

        private void RemoveDefenseBox_Click(object sender, EventArgs e)
        {
            int SelectedIndex = DefenseListBox.SelectedIndex;
            if (SelectedIndex >= 0)
                DefenseListBox.Items.RemoveAt(SelectedIndex);
            if (SelectedIndex < DefenseListBox.Items.Count)
                DefenseListBox.SelectedIndex = SelectedIndex;
            else
            {
                SelectedIndex -= 1;
                if (SelectedIndex < DefenseListBox.Items.Count)
                    DefenseListBox.SelectedIndex = SelectedIndex;
            }
        }

        private void SubtractOffenseBox_Click(object sender, EventArgs e)
        {
            int Count = 0;
            if (int.TryParse(CountBox.Text, out Count) && Count >= 0 && OffenseListBox.SelectedIndex >= 0)
            {
                FastSim.Unit Unit = (FastSim.Unit)OffenseListBox.SelectedItem;
                Unit.Count -= Count;
                int SelectedIndex = OffenseListBox.SelectedIndex;
                if (Unit.Count > 0)
                    OffenseListBox.Items[SelectedIndex] = Unit;
                else
                {
                    OffenseListBox.Items.RemoveAt(SelectedIndex);
                    if (SelectedIndex < OffenseListBox.Items.Count)
                        OffenseListBox.SelectedIndex = SelectedIndex;
                    else
                    {
                        SelectedIndex -= 1;
                        if (SelectedIndex < OffenseListBox.Items.Count)
                            OffenseListBox.SelectedIndex = SelectedIndex;
                    }
                }
            }
        }

        private void SubtractDefenseBox_Click(object sender, EventArgs e)
        {
            int Count = 0;
            if (int.TryParse(CountBox.Text, out Count) && Count >= 0 && DefenseListBox.SelectedIndex >= 0)
            {
                FastSim.Unit Unit = (FastSim.Unit)DefenseListBox.SelectedItem;
                Unit.Count -= Count;
                int SelectedIndex = DefenseListBox.SelectedIndex;
                if (Unit.Count > 0)
                    DefenseListBox.Items[SelectedIndex] = Unit;
                else
                {
                    DefenseListBox.Items.RemoveAt(SelectedIndex);
                    if (SelectedIndex < DefenseListBox.Items.Count)
                        DefenseListBox.SelectedIndex = SelectedIndex;
                    else
                    {
                        SelectedIndex -= 1;
                        if (SelectedIndex < DefenseListBox.Items.Count)
                            DefenseListBox.SelectedIndex = SelectedIndex;
                    }
                }
            }
        }

        private void OffenseListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (OffenseListBox.SelectedIndex >= 0)
            {
                FastSim.Unit Offense = (FastSim.Unit)OffenseListBox.SelectedItem;
                string Name = UnitListBox.SelectedItem != null ? ((FastSim.Unit)UnitListBox.SelectedItem).Name : "null";
                if (Offense.Name != Name)
                {
                    for (int i = 0; i < UnitListBox.Items.Count; i++)
                    {
                        FastSim.Unit Unit = (FastSim.Unit)UnitListBox.Items[i];
                        if (Offense.Name == Unit.Name)
                        {
                            UnitListBox.SelectedIndex = i;
                            return;
                        }
                    }
                    UnitListBox.SelectedIndex = -1;
                }
            }
        }

        private void DefenseListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DefenseListBox.SelectedIndex >= 0)
            {
                FastSim.Unit Defense = (FastSim.Unit)DefenseListBox.SelectedItem;
                string Name = UnitListBox.SelectedItem != null ? ((FastSim.Unit)UnitListBox.SelectedItem).Name : "null";
                if (Defense.Name != Name)
                {
                    for (int i = 0; i < UnitListBox.Items.Count; i++)
                    {
                        FastSim.Unit Unit = (FastSim.Unit)UnitListBox.Items[i];
                        if (Defense.Name == Unit.Name)
                        {
                            UnitListBox.SelectedIndex = i;
                            return;
                        }
                    }
                    UnitListBox.SelectedIndex = -1;
                }
            }
        }

        private void UnitListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (UnitListBox.SelectedIndex >= 0)
            {
                FastSim.Unit Unit = (FastSim.Unit)UnitListBox.SelectedItem;

                bool Found = false;
                if (OffenseListBox.SelectedIndex >= 0)
                {
                    FastSim.Unit Offense = (FastSim.Unit)OffenseListBox.SelectedItem;
                    if (Unit.Name == Offense.Name)
                        Found = true;
                }
                for (int i = 0; i < OffenseListBox.Items.Count; i++)
                {
                    FastSim.Unit Offense = (FastSim.Unit)OffenseListBox.Items[i];
                    if (Unit.Name == Offense.Name)
                    {
                        OffenseListBox.SelectedIndex = i;
                        Found = true;
                    }
                }
                if (!Found) OffenseListBox.SelectedIndex = -1;

                Found = false;
                if (DefenseListBox.SelectedIndex >= 0)
                {
                    FastSim.Unit Defense = (FastSim.Unit)DefenseListBox.SelectedItem;
                    if (Unit.Name == Defense.Name)
                        Found = true;
                }
                for (int i = 0; i < DefenseListBox.Items.Count; i++)
                {
                    FastSim.Unit Defense = (FastSim.Unit)DefenseListBox.Items[i];
                    if (Unit.Name == Defense.Name)
                    {
                        DefenseListBox.SelectedIndex = i;
                        Found = true;
                    }
                }
                if (!Found) DefenseListBox.SelectedIndex = -1;
            }
        }

        //Simulation Simulation;
        bool ContinueSimulation = false;

        private void RunBox_Click(object sender, EventArgs e)
        {
            if (RunBox.Text == "Run")
            {
                OffenseListBox.Enabled = DefenseListBox.Enabled = UnitListBox.Enabled =
                    RemoveOffenseBox.Enabled = SubtractOffenseBox.Enabled = AddOffenseBox.Enabled =
                    CountBox.Enabled =
                    RemoveDefenseBox.Enabled = SubtractDefenseBox.Enabled = AddDefenseBox.Enabled = false;
                RunBox.Text = "Stop";
                ContinueSimulation = true;
                new System.Threading.Thread(RunSimulation).Start();
            }
            else if (ContinueSimulation) ContinueSimulation = false;
        }
        FastSim FastSim = new FastSim();
        private bool OffenseChanged = false, DefenseChanged = false;
        private void RunSimulation()
        {
            FastSim.Offense = new FastSim.Player() { StartingUnits = OffenseListBox.Items.Cast<FastSim.Unit>().ToArray() };
            FastSim.Defense = new FastSim.Player() { StartingUnits = DefenseListBox.Items.Cast<FastSim.Unit>().ToArray() };
            FastSim.Reset();
            DateTime LastUpdate = new DateTime(1, 1, 1);
            TimeSpan UpdateSpan = new TimeSpan(0, 0, 1);
            int Battles = 0;
            while (ContinueSimulation)
            {
                Battles++;
                FastSim.RunBattle();
                if (DateTime.Now - LastUpdate > UpdateSpan)
                {
                    LastUpdate = DateTime.Now;
                    Invoke(new UpdateDelegate(Update), Battles);
                }
            }
            Invoke(new UnlockDelegate(Unlock));

        }
        delegate void UpdateDelegate(int Battles);
        void Update(int Battles)
        {
            OffenseCalculationList.Items.Clear();
            FastSim.Unit[] Units = FastSim.Offense.AverageBattleEnd(Battles);
            long CST = 0, CPX = 0, AT = 0, U = 0;
            foreach (FastSim.Unit Unit in Units)
            {
                OffenseCalculationList.Items.Add(Unit);
                int LossCount = (int)Math.Round(Unit.Lost);
                U += LossCount;
                CST += (long)(Unit.CST * LossCount);
                CPX += (long)(Unit.CPX * LossCount);
                AT += (long)(Unit.AT * LossCount);
            }
            int AvgTurns = FastSim.Offense.Wins > 0 ? (int)Math.Round((decimal)FastSim.Offense.WinTurns / FastSim.Offense.Wins) : 0;
            OffenseSummaryLabel.Text = $"T: {AvgTurns} -ULT: {CST} -CPX: {CPX} -AT: {AT} -U: {U} W: {FastSim.Offense.Wins} W%: {Math.Round(((decimal)FastSim.Offense.Wins / FastSim.Battles) * 100)}";
            DefenseCalculationList.Items.Clear();
            Units = FastSim.Defense.AverageBattleEnd(Battles);
            CST = CPX = AT = 0;
            foreach (FastSim.Unit Unit in Units)
            {
                DefenseCalculationList.Items.Add(Unit);
                int LossCount = (int)Math.Round(Unit.Lost);
                U += LossCount;
                CST += (long)(Unit.CST * LossCount);
                CPX += (long)(Unit.CPX * LossCount);
                AT += (long)(Unit.AT * LossCount);
            }
            AvgTurns = FastSim.Defense.Wins > 0 ? (int)Math.Round((decimal)FastSim.Defense.WinTurns / FastSim.Defense.Wins) : 0;
            DefenseSummaryLabel.Text = $"T: {AvgTurns} -ULT: {CST} -CPX: {CPX} -AT: {AT} -U: {U} W: {FastSim.Defense.Wins} W%: {Math.Round(((decimal)FastSim.Defense.Wins / FastSim.Battles) * 100)}";
        }
        delegate void UnlockDelegate();
        void Unlock()
        {
            OffenseListBox.Enabled = DefenseListBox.Enabled = UnitListBox.Enabled =
                    RemoveOffenseBox.Enabled = SubtractOffenseBox.Enabled = AddOffenseBox.Enabled =
                    CountBox.Enabled =
                    RemoveDefenseBox.Enabled = SubtractDefenseBox.Enabled = AddDefenseBox.Enabled = true;
            RunBox.Text = "Run";
        }
    }
}
